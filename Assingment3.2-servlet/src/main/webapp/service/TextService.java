package main.webapp.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TextService {

    public void WriteToFile(String title, String message) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(title + ".txt"));
        writer.write(message);
        writer.close();
    }


}
