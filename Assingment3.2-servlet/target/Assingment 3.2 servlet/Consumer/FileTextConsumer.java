package main.webapp.consumer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import main.webapp.entities.DVD;
import main.webapp.service.TextService;
import org.apache.commons.lang3.SerializationUtils;

public class FileTextConsumer {

    private static final String EXCHANGE_NAME = "logs";



    public static void main(String[] argv) throws Exception {

        TextService textService = new TextService();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {

            DVD dvd = SerializationUtils.deserialize(delivery.getBody());
            String title = dvd.getTitle();
            String message = dvd.toString();
            System.out.println(" [x] Received '" + dvd + "'");

            textService.WriteToFile(title, message);
        };

        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
        });
    }
}
