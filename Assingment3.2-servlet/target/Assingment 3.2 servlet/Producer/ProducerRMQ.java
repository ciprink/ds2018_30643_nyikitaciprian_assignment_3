package main.webapp.Producer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import main.webapp.entities.DVD;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import main.webapp.service.MailService;
import org.apache.commons.lang3.SerializationUtils;

public class ProducerRMQ extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("index.html").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DVD dvd = new DVD();

        String title = request.getParameter("title");
        int year = Integer.parseInt(request.getParameter("year"));
        double price = Double.parseDouble(request.getParameter("price"));


        if(title != null && year>=0 && price>=0) {
            dvd.setTitle(title);
            dvd.setYear(year);
            dvd.setPrice(price);
            System.out.println("Se trimite: " + dvd.toString());


            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {
                channel.exchangeDeclare("logs", "fanout");

                String message = dvd.toString();

                channel.basicPublish("logs", "", null, SerializationUtils.serialize(dvd));
                System.out.println(" [x] Sent '" + message + "'");

                MailService mailService = new MailService("cosmin.nyikita@gmail.com", "dummypassword");
                mailService.sendMail("cosmin.nyikita@gmail.com", "New DVD :)", message);

            } catch (TimeoutException e) {
                e.printStackTrace();
            }

        }
        request.getRequestDispatcher("index.html").forward(request,response);
    }


}
